#ifndef Article_h
#define Article_h

#include <iostream>
#include <stdlib.h>
#include <sstream>


using namespace std;
class Article{
	public:
		Article(string Name, string Prize, string Quantity);
		string Name;
		string Prize;
		string Quantity;
		void ShowData();
};


#endif
