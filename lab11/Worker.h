#ifndef WORKER_h
#define WORKER_h

#include <iostream>
#include <stdlib.h>
#include <sstream>
using namespace std;

class Worker{
	public:
		Worker(string Name, string Surname, string Profession, string Pesel);
		string Name;
		string Surname;
		string Profession;
		string Pesel;		
		void ShowData();
};


#endif
