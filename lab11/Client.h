#ifndef Client_h
#define Client_h


#include <iostream>
#include <stdlib.h>
#include <sstream>

using namespace std;

class Client{
	public:
		Client(string Name, string Surname, string ClientCardNumber);
		string Name;
		string Surname;
		string ClientCardNumber;
		void ShowData();
};


#endif
