#include "Repository.h"



Repository&  Repository::operator +=(const Worker &worker){
	this->workers.push_back(worker);
	return *this;
}



Repository& Repository::operator +=(const Article &article){
	this->articles.push_back(article);
	return *this;
}



Repository& Repository::operator +=(const Client &client){
	this->clients.push_back(client);
	return *this;
}



void Repository::add(Worker w){
	this->workers.push_back(w);
}


void Repository::add(Article a){
	this->articles.push_back(a);
}

void Repository::add(Client c){
	this->clients.push_back(c);
}




void Repository::show(int i){
	system("cls");
	switch(i){
	
	
case 1:	cout<<"Lista pracowników:"<<endl; 
	for(int i=0; i<workers.size();i++){
		Worker s = workers.at(i);
		cout<<i+1<<".";
		s.ShowData();
	} break;
	
	
case 2: cout<<"Lista Artykułów:"<<endl;
	 for(int i=0; i<articles.size();i++){
		Article a = articles.at(i);
		cout<<i+1<<".";
		a.ShowData();
	} break;
	
	
case 3: cout<<"Lista Klientów:"<<endl;
	 for(int i=0; i<clients.size();i++){
		Client c = clients.at(i);
		cout<<i+1<<".";
		c.ShowData();
	} break;
	
}
}


void Repository::del(int n, string x){
	switch(n){
	
	case 1:
		for(int i=0; i<this->workers.size();i++){
		if( this->workers.at(i).Pesel==x){
			this->workers.erase(this->workers.begin()+i);
		}
	};break;
	
	case 2:
	for(int i=0; i<this->articles.size();i++){
		if(this->articles.at(i).Name==x){
			this->articles.erase(this->articles.begin()+i);
		}
	};break;
	
	case 3:
		for(int i=0; i<this->clients.size();i++){
		if(this->clients.at(i).ClientCardNumber==x){
			this->clients.erase(this->clients.begin()+i);
		}
	};break;
		
	
}
}


