#ifndef Menu_h	
#define Menu_h
#include <iostream>
#include <cstdlib>
#include <string>
#include <conio.h>
#include "Repository.h"
#include "Form.h"



class Menu{
	public:
		void MainMenu();
		void SelectOption(int c);	
		void SelectAdd();
		void SelectDel();
		void SelectShow();
		
		Form wpr;
};



#endif


