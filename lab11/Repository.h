#ifndef Repository_h
#define Repository_h
#include <vector>
#include "Worker.h"
#include "Client.h"
#include "Article.h"

using namespace std;
class Repository {
	public:
		
		vector <Worker> workers;
		vector <Article> articles;
		vector <Client> clients; 
					
		Repository& operator +=(const Worker &worker);
		Repository& operator +=(const Article &article);
		Repository& operator +=(const Client &client);
						
		void add(Worker w);
		void add(Article a);
		void add(Client c);
						
		void del(int n, string x);
		void show(int i);
};

#endif
