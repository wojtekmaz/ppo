<?php

require "./vendor/autoload.php";


use Scheduler\Models\Lecture;
use Scheduler\Models\Lab;
use Scheduler\Models\Schedule;
use Scheduler\Models\Discussions;
use Scheduler\Models\Seminar;
use Scheduler\Models\Project;
use Scheduler\Models\WF;
use Scheduler\Models\NewSpecialEvent;



$twig = new Twig_Environment(new Twig_Loader_Filesystem("/"), []);


$schedule = new Schedule();

$schedule->save(new Lecture("Bazy danych","dr Aleksander Klosow","C","212"), 1, 1);
$schedule->save(new WF("","Łukasz Stawarz","A","42"), 1, 2);

$schedule->save(new Lab("Projektowanie i programowanie obiektowe","mgr inż. Krzysztof Rewak","C","10"), 1, 5);

$schedule->save(new NewSpecialEvent("Konferencja",""),2,3);
$schedule->save(new Project("Projektowanie i programowanie obiektowe","mgr inż. Krzysztof Rewak","C","10"), 2, 5);
$schedule->save(new Lab("Bazy danych","mgr inż. Józefa Górska-Zając","A","212"), 2, 6);

$schedule->save(new Lecture("Podstawy metod probabilistycznych i statystyki","dr Karol Selwat","C","212"), 3, 1);

$schedule->save(new Project("Projektowanie i programowanie obiektowe","mgr inż. Krzysztof Rewak","C","10"), 4, 3);
$schedule->save(new Lab("Bazy danych","mgr inż. Józefa Górska-Zając","A","212"), 4, 4);

$schedule->save(new Seminar("Praktyka projektowania systemów HVAC. Wpływ nowych przepisów na dobór urządzeń. Nowoczesne rozwiązania produktowe.","","E","1"), 5, 5);


echo $twig->render("index.twig", [
    "schedule" => $schedule,
]);


	

		
