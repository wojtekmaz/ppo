<?php

namespace Scheduler\Interfaces;

interface EventInterface {

	public function getName(): string;
	public function getColor():string;
	public function getKind():string; 
	
	

}
