<?php

namespace Scheduler\Models;

use Scheduler\Interfaces\EventInterface;

abstract class SemesterClass implements EventInterface {

	private $name;
	private $lecturer;
	private $building;
	private $room;
	
	

	public function __construct(string $name, string $lecturer, string $building, string $room) {
		$this->name = $name;
		$this->lecturer = $lecturer;
		$this->building = $building;
		$this->room = $room;
		
	}
	
	public function getName(): string {
		return $this->name;
	}
	
	public function getLecturer(): string {
		return $this->lecturer;
	}
	
	public function getRoom(): string {
		return $this->room;
	}
	
	public function getBuilding(): string {
		return $this->building;
	}
	
	

}
