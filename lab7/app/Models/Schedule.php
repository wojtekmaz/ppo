<?php

namespace Scheduler\Models;

use Scheduler\Interfaces\EventInterface as Event;


class Schedule {

	private $event = [[]];
	
	
	private static $week_day_names = [
		"1" => "Poniedziałek",
		"2" => "Wtorek",
		"3" => "Środa",
		"4" => "Czwartek",
		"5" => "Piątek",
	];

	private static $hour_slots = [
		"1" => "08:15-09:45",
		"2" => "10:00-11:30",
		"3" => "11:45-13:15",
		"4" => "13:30-15:00",
		"5" => "15:15-16:45",
		"6" => "17:00-18:30",
		"7" => "18:45-20:15",
	];
	
	
	

	public function getDays() {
		return self::$week_day_names;
	}
		
	public function getHours() {
		return self::$hour_slots;
	}

	public function hasEvent(int $week_day, int $hour_slot): bool {
		return isset($this->event[$week_day][$hour_slot]);
	}

	public function getEvent(int $week_day, int $hour_slot): Event {
		return $this->event[$week_day][$hour_slot];
	}
	

	public function save(Event $event, int $week_day, int $hour_slot) {
		$this->event[$week_day][$hour_slot] = $event;
	}
	

}
