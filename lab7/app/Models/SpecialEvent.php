<?php

namespace Scheduler\Models;

use Scheduler\Interfaces\EventInterface;

abstract class SpecialEvent implements EventInterface {

	private $name;
	private $desc;
	

	public function __construct(string $name,string $desc) {
		$this->name = $name;
		$this->desc = $desc;
		
	}
	
	public function getName(): string {
		return $this->name;
	}
	
	
	
	public function getDesc(): string {
		return $this->desc;
	}			
	 
	

}
