#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>

using namespace std;

#define STUDENTS_COUNT 10

class Student {
	public:
		string studentNo;
		string studentName;
		string studentSurname;
		bool studentActive;
			
		void setStudentNo(string studentNo) {
			this->studentNo = studentNo;}
		
		string getStudentNo() {
			return this->studentNo;}
		
		void setStudentName(string studentName) {
			this->studentName = studentName;}
		
		string getStudentName() {
			return this->studentName;}
		
		void setStudentSurname(string studentSurname) {
			this->studentSurname = studentSurname;}
		
		string getStudentSurname() {
			return this->studentSurname;}
		
		void setStudentActive(bool studentActive){
			this->studentActive = studentActive;}
		
		bool getStudentActive(){
			return this->studentActive;}
};

string getRandomStudentNumber() {
	ostringstream ss;
	int randomNumber = rand() % 2000 + 37000;
	ss << randomNumber;
	return ss.str();
}

string getRandomName(){
	std::string Name[ 4 ];
	Name[ 0 ] = "Szymon";
	Name[ 1 ] = "Antek";
	Name[ 2 ] = "Piotr";
	Name[ 3 ] = "Kazimierz";
	
	int randomName = rand() % 4;
	return Name[randomName];
}

string getRandomSurname(){
	std::string surName[ 4 ];
	surName[ 0 ] = "Kluczkowski";
	surName[ 1 ] = "Pisarz";
	surName[ 2 ] = "Radomczyk";
	surName[ 3 ] = "Renifer";
	
	int randomsurName = rand() % 4;
	return surName[randomsurName];
}

int getRandomActive(){
	int randomActive= rand()%2;
	return randomActive;
}


int main() {
	vector<Student> students;	
	for(int i = 0; i < STUDENTS_COUNT; i++) {
		Student student;
		
		student.setStudentNo(getRandomStudentNumber());
		student.setStudentName(getRandomName());
		student.setStudentSurname(getRandomSurname());
		student.setStudentActive(getRandomActive());
		
		students.push_back(student);
	}
	
	cout  << "Students group have been filled." << endl << endl;
	
	for(int i = 0; i < students.size(); i++) {
		Student student = students.at(i);
		

		cout << student.getStudentName()<<" "<< student.getStudentSurname()<<" "<<student.getStudentNo()<<" "<<student.getStudentActive() << endl;
	}
	
	return 0;
}
