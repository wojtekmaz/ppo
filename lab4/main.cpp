#include <iostream>
#include <cstdlib>
#include <string>
#include <sstream>
#include <vector>
#include <iomanip>
#include <conio.h>

using namespace std;


class Student{
	public: 
	string Name;
	string Surname;
	long long int PESEL;
	int NumIndeks;
	string Address;
	int YearOfStudy;
	

	void setStudent(string Name, string Surname, long long int PESEL, int NumIndeks){
		this->Name = Name;
		this->Surname = Surname;
		this->PESEL = PESEL;
		this->NumIndeks = NumIndeks;
	}
	
	void setStudentAddress(){
		this->Address = "nie wprowadzono adresu";
	}
	
	void setStudentAddress(string Address){
		this->Address = Address;
	}
	
	void setStudentYear(){
		this->YearOfStudy = 0;
	}
	
	void setStudentYear(int YearOfStudy){
		this->YearOfStudy = YearOfStudy;
	}
	
	string getName(){
		return this->Name;
	}
	
	string getSurname(){
		return this->Surname;
	}
	
	long long int getPESEL(){
		return this->PESEL;
	}
	
	int getNumIndeks(){
		return this->NumIndeks;
	}
	
	string getAddress(){
		return this->Address;
	}
	
	int getYearOfStudy(){
		return this->YearOfStudy;
	}

};

vector<Student> students;

void menu(){
	system( "cls" );
	cout << "a) Wprowadz nowego studenta" << endl;
	cout << "b) Wyswietl liste studentow" << endl;
	cout << "c) Wyjdz" << endl;
}
int checkPESEL(long long int PESEL){
	if((PESEL>9999999999)&&(PESEL<100000000000)){
	int d=(PESEL/100000)%100;
	int m=(PESEL/10000000)%100;
	int y=(PESEL/1000000000);
	if((d>0&&d<=31)&&(m>0&&m<=12)&&(y>=60)){
		return 1;
	}
	else{
		return 0;
	}
	}
	else{
		return 0;
	}
}

int checkAddress(string Address){
	if(Address!="\0"){
		return 1;
	}
	else{ 
		return 0;
	}
}

int checkYearOfStudy(int Year){
	if(Year>0&&Year<6){
		return 2;
	}
	if(Year>5||Year<=0){
		return 3;
	}
}

int checkNumIndeks(int NumIndeks){
	if(NumIndeks<20000){
		return 0;
	}
	else{ return 1;
	}
}

int checkData(string Name, string Surname){
	if((Name!="\0")&&(Surname!="\0")){
		return 1;
	}
	else return 0;
}

string getYearOfStudy(int StudyYear){
	string Year[6];
	Year[ 0 ] = "Nie wprowadzono roku";
	Year[ 1 ] = "I rok";
	Year[ 2 ] = "II rok";
	Year[ 3 ] = "III rok";
	Year[ 4 ] = "IV rok";
	Year[ 5 ] = "V rok";
	
	
	return Year[StudyYear];
}

void AddStudent(){
	cout << endl;
	string Name;
	string Surname;
	long long int PESEL;	
	int NumIndeks;
	string Address;
	int YearOfStudy;
	Student student;
	cout << "Podaj Imie:" <<endl;
	cin.sync();
	getline(cin, Name);
	cout << "Podaj Nazwisko:" <<endl;
	cin.sync();
	getline(cin, Surname);
	cout << "Podaj Pesel:" <<endl;
	cin >> PESEL;
	cout << "Podaj Indeks:" <<endl;
	cin >> NumIndeks;
	cout << "Podaj Adres:" <<endl;
	cin.sync();
	getline(cin, Address);
	cout << "Podaj Rok:" <<endl;
	cin >> YearOfStudy;
	
	
	if(checkPESEL(PESEL)){
	if(checkNumIndeks(NumIndeks)){		
	if(checkData(Name,Surname)){
		
		
		student.setStudent(Name,Surname,PESEL,NumIndeks);
		cout << "Student dodany" << endl;
		
		if(checkAddress(Address)){
			student.setStudentAddress(Address);
		}else{
			student.setStudentAddress();
		}
		
		switch(checkYearOfStudy(YearOfStudy)){
			case 2: student.setStudentYear(YearOfStudy); break;
			case 3: student.setStudentYear(); cout << "Rok nie zostal wprowadzony - bledny rok" << endl; break;
		}
	
		students.push_back(student);
		
	}else{
		cout << "Student niedodany - brak wymaganych danych"<< endl;
	}
	
	}else{
		cout << "Student niedodany - bledny indeks" << endl;
	}
	
	}else{
		cout << "Student niedodany - bledny PESEL" << endl;
	}
	system("PAUSE");
	return;
}

void ShowStudents(){
	cout << endl;
	cout<< setw(15) <<"Imie; "<<"Nazwisko; "<<"PESEL; "<<"Indeks; "<<"Rok; "<<"Adres; "<< endl;
	for(int i = 0; i < students.size(); i++) {
		Student student = students.at(i);
		cout << setw(15)<< student.getName() << "; " << student.getSurname() << "; " << student.getPESEL() << "; " << student.getNumIndeks() << "; " << getYearOfStudy(student.getYearOfStudy()) << "; " << student.getAddress() << ";" << endl;
	}
	system("PAUSE");
}

int main(int argc, char** argv) {
	
	char c;
	do{
		menu();
	 	c=getche();
		switch(c)
		{
		case 'a': case 'A': AddStudent(); break;
		case 'b': case 'B': ShowStudents(); break;
		case 'c': case 'C': break;
		}
	}while(c!='c'&&c!='C');
		
	return 0;
}
